const express = require("express");
const router = express.Router();
const bodyParse = require("body-parser");


let datos =[{
    matricula:"2020030806",
    nombre:"Francisco Vega Hernandez",
    sexo:"M",
    materia:["ingles","Tegnologia","Base de datos"],
   },{
       matricula:"2020020309",
       nombre:"ALMOGAR VAZQUEZ YARLEN DE JESUS",
       sexo:"F",
       materia:["ingles","Tegnologia","Base de datos"],
   },{
       matricula:"2020020001",
       nombre:"ACOSTA ORTEGA JESUS HUMBERTO",
       sexo:"M",
       materia:["ingles","Tegnologia","Base de datos"],
   
   },{
       matricula:"2020020890",
       nombre:"LUIS GUILLERMO CHAVEZ ORTIZ",
       sexo:"M",
       materia:["ingles","Tegnologia","Base de datos"],
   },{
       matricula:"2020020721",
       nombre:"MARIANA DE JESUS HERNANDEZ COLIO",
       sexo:"F",
       materia:["ingles","Tegnologia","Base de datos"],
   },{
       matricula:"2020020691",
       nombre:"MAKAKARDO VEGA HERNANDEZ",
       sexo:"M",
       materia:["ingles","Tegnologia","Base de datos"],
   }
   
   ]
   
   
   router.get('/',(req,res)=>{
       
       res.render('index.html',{titulo:"Listado de Alumnos",listado:datos})
       //res.send("<h1>Iniciamos con express<h1>");
   
   })

   router.get("/cotizacion",(req,res)=>{
    const valores = {
        valor:req.query.valor,
        pInicial:req.query.pInicial,
        plazos:req.query.plazos
    }
})


router.post("/cotizacion",(req,res)=>{
    const valores = {
        valor:req.body.valor,
        pInicial:req.body.pInicial,
        plazos:req.body.plazos
    }
    res.render('cotizacion.html', valores);
})

router.get("/tablas",(req,res)=>{
    const valores = {
        tabla:req.query.tabla,
       
    }
    res.render('tablas.html', valores);
})

router.post("/tablas",(req,res)=>{
    const valores = {
        tabla:req.body.tabla,
        
    }
    res.render('tablas.html', valores);
})


   module.exports=router;

   router.get("/boletos",(req,res)=>{
    const valores = {
        tabla:req.query.tabla,
       
    }
    res.render('boletos.html', valores);
})

router.post("/boletos",(req,res)=>{
    const valores = {
        tabla:req.body.tabla,
        
    }
    res.render('boletos.html', valores);
})

