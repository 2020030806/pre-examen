const hhtp = require("http");
const express = require("express");
const app = express();
const misRurtas=require("./router/index")
const bodyparser = require("express");
const path=require("path")


app.set('view engine', "ejs");
app.use(express.static(__dirname + '/public'));
app.use(bodyparser.urlencoded({extended:true}));
app.engine('html',require('ejs').renderFile);
app.use(misRurtas);
// escuchar comentarios dels evidor por el puerto 3000

const puerto = 500;

app.use((req,res,next)=>{
    res.status(404).sendFile(__dirname + '/views/error.html')
})
app.listen(puerto,()=>{

    console.log("Iniciado puerto 500");
    
})